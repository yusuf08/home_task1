<!DOCTYPE html>

<html>

<head>
    <title>PHP Tutorial</title>
    <style>
        body{max-width:900px; margin:auto;}
        .phpcoding{ width: 80%; margin: 0 auto;
            background-color: darkgray;
            background-repeat:no-repeat;
            background-attachment: fixed;
            background-position:0px 0px;
            background-min-height:600px;
            background-size:100%;
            padding: 20px;
        //animation: animate-background linear 50s infinite;
            color: black;font-family: "Arial Black";
        }
        .headeroption, .footeroption{ background-color: black; text-align: center; color:azure; opacity:0.5; filter: alpha(opacity=100); padding : 20px;}
        .maincontent{ text-align: left;color:#000; min-height: 700px; padding : 20px;}

    </style>

</head>

<body>
<div class = "phpcoding">
    <section class= "headeroption">
        <h2>Day 07 Home Task</h2>

    </section>

    <section class = "maincontent">
        <br><br>

        <?php
        //*********Code for findings Odd numbers between 1-10************************************************************************
        echo"<hr><h3>Odd Numbers between 1-10:</h3>&nbsp&nbsp";
        for($i=1;$i<=10;$i++){

            if($i%2!=0){
                echo $i.",";
            }
        }

        //*********Code for Finding Even Numbers between 1-10;************************************************************************
        echo"<br><br><hr><h3>Even Numbers between 1-10 : &nbsp&nbsp </h3><br>";
        for($i=1;$i<=10;$i++) {

            if ($i % 2 == 0) {
                echo $i . ",";
            }
        }

        //*********Code for Star Pattern #1************************************************************************
        echo"<br><br><hr><h3>First Pattern</h3><br>";
        $n1=2;
        for($i=1;$i<=$n1;$i++)
        {
            for($j=1;$j<=$i;$j++)
            {
                echo "*";
            }
            echo '<br>';
            echo " ";
        }
        for($i=$n1;$i>=1;$i--){

            for($j=1;$j<=$i-1;$j++){
                echo "*";
            }
            echo "<br>";
        }

        //****************Code for star pattern #2 ************************************************
        echo"<br><br><hr><h3>Second Pattern</h3><br>";
        $n1=2;
        for($i=1;$i<=$n1;$i++)
        {
            for($j=1;$j<=$i+1;$j++)
            {
                echo "*";
            }
            echo '<br>';
        }

        for($i=$n1;$i>1;$i--){

            for($j=1;$j<=$i;$j++){
                echo "*";
            }
            echo "<br>";
        }
        ?>

    </section>



    <section class= "footeroption">
        <h4>Home Task 2/2</h4>
    </section>
</div>

</body>

</html>
